#include "Nodo.h"
#include "Lista.h"
#include "Ejer_3.h"
#include <string>
#include <iostream>
using namespace std;

int main (void) {
    Ejercicio e = Ejercicio();
    Lista *lista = e.get_lista();
    string in;
    int num, temp = 0;
    
    while(temp == 0){
		cout<<"Ingrese un numero entero:"<<endl;
		cout<<"   ~~~> ";
		getline(cin, in);
		num = stoi(in);
    
		lista->crear(num);    
		lista->imprimir();
		
		cout<<"Para ingresar otro numero ingrese 0"<<endl;
		cout<<"Para salir ingrese cualquier numero"<<endl;
		cout<<"   ~~~> ";
		
		getline(cin, in);
		temp = stoi(in);
		
	}
	
    return 0;
}
