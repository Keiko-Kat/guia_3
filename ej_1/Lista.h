#include <iostream>
#include "Nodo.h"
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Lista();
        
        /* crea un nuevo nodo, recibe una instancia de la clase Persona. */
        void crear (int numero);
        /* imprime la lista. */
        void imprimir ();
};
#endif
