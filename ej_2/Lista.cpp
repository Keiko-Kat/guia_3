#include <iostream>
using namespace std;

#include "Nodo.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::crear (int numero) {
    Nodo *tmp;
    Nodo *temp = this->raiz;
	Nodo *repl;

    /* crea un nodo . */
    tmp = new Nodo;
    /* asigna la instancia de Numero. */
    tmp->numero = numero;
    /* apunta a NULL por defecto. */
    tmp->sig = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, se compara con el resto de los numeros de la lista. */
    }else {		
		while (temp != NULL) {
			/*si es menor al primero se inserta en su lugar*/
			if(tmp->numero < temp->numero){
				this->raiz = tmp;
				this->raiz->sig = temp;
				temp = NULL; break;
			}else{
				if(tmp->numero > temp->numero){					
					if(temp->sig != NULL){
						repl = temp->sig;
						if(tmp->numero < temp->sig->numero){
							temp->sig = tmp;
							tmp->sig = repl;
							temp = NULL; break;
						}
					}else{
						this->ultimo->sig = tmp;
						this->ultimo = tmp;
						temp = NULL; break;
					}
					
					temp = temp->sig;
				}
			}
		}
	}
}

void Lista::imprimir () {  
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;
	cout<<"Numero: ";
    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    while (tmp != NULL) {
        cout << tmp->numero <<" ";
        tmp = tmp->sig;
    }
    cout<< endl;
}
