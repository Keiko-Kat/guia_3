#include "Nodo.h"
#include "Lista.h"
#include "Ejer_2.h"
#include <string>
#include <iostream>
using namespace std;

int main (void) {
    Ejercicio e = Ejercicio();
    Lista *lista_1 = e.get_lista_1();
    Lista *lista_2 = e.get_lista_2();
    Lista *lista_fin = e.get_lista_fin();
    string in;
    int num, temp = 0, opt = 0;
    
    while(temp == 0){
		
		cout<<"Si desea ingresar un numero en la lista 1 ingrese [1]"<<endl;
		cout<<"Si desea ingresar un numero en la lista 2 ingrese [2]"<<endl;
		getline(cin, in);
		opt = stoi(in);
		
		switch(opt){
			case 1:	cout<<"Ingrese un numero entero:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);
					num = stoi(in);
    
					lista_1->crear(num);
					lista_fin->crear(num);  	
					break;
		
			case 2:	cout<<"Ingrese un numero entero:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);
					num = stoi(in);
							
					lista_2->crear(num);    
					lista_fin->crear(num);
					break;
			default: cout<<"la opcion ingresada no es valida"<<endl;
			
		}
		cout<<"Lista 1: "<<endl;  
		lista_1->imprimir();
		cout<<endl;
		cout<<"Lista 2: "<<endl;
		lista_2->imprimir();
		cout<<endl;
		cout<<"Lista mezclada: "<<endl;
		lista_fin->imprimir();
		cout<<endl;
		cout<<"Para ingresar otro numero ingrese 0"<<endl;
		cout<<"Para salir ingrese cualquier numero"<<endl;
		cout<<"   ~~~> ";
		
		getline(cin, in);
		temp = stoi(in);
		
	}
	
    return 0;
}
