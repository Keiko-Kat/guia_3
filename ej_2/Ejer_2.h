#include "Nodo.h"
#include "Lista.h"
using namespace std;



class Ejercicio {
    private:
        Lista *lista_1 = NULL;
        Lista *lista_2 = NULL;
        Lista *lista_fin = NULL;

    public:
        /* constructor */
        Ejercicio() {
            this->lista_1 = new Lista();
            this->lista_2 = new Lista();
            this->lista_fin = new Lista();
        }
        
        Lista *get_lista_1() {
            return this->lista_1;
        }
        Lista *get_lista_2() {
            return this->lista_2;
        }
        Lista *get_lista_fin() {
            return this->lista_fin;
        }
};
